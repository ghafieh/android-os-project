#include <stdio.h>
#include <sys/time.h>

int main()
{
	struct timeval my_time;

	gettimeofday(&my_time, NULL);

	printf("%ld seconds, %ld microseconds\n", my_time.tv_sec, my_time.tv_usec);

	return 0;
}
