#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>


char *read_single_line(void);
char **parse_line(char *line);
int execute(char **tokens);
int execute_program(char **tokens);
int execute_builtins(char **tokens);

int main(int argc, char **argv) {
    int exit;
    char *line;
    char **splitted_line;
    int start = 0;

    while (exit || start == 0) {
        printf("> ");
        line = read_single_line();
        splitted_line = parse_line(line);

        exit = execute(splitted_line);       //check what output and when
        free(line);
        free(splitted_line);
        start++;
    }

    return EXIT_SUCCESS;
}



char *read_single_line(void) {
    int buffer_size = 1024;
    char *buffer = malloc(sizeof(char) * buffer_size);
    int position_index = 0;
    int c;              //EOF is integer not character
    while (1) {
        c = getchar();
        if (c == EOF || c == '\n') {
            buffer[position_index] = '\0';
            return buffer;                  //line finished
        }
        else {
            buffer[position_index] = c;
        }
        position_index++;
    }
}

char **parse_line(char *line) {
    int buffer_size = 64;
    int position_index = 0;
    char **tokens = malloc(sizeof(char*) * buffer_size);
    char *single_token;
    char *whitespace = " \t\n";

    single_token = strtok(line, whitespace);
    while (single_token != NULL) {
        tokens[position_index] = single_token;
        position_index++;
        single_token = strtok(NULL, whitespace);        //why?
    }
    tokens[position_index] = NULL;
    return tokens;
}

int execute(char **tokens) {
    int i;
    if (tokens[0] == NULL) {
        return 1;
    }

    if (strcmp(tokens[0], "cat") == 0 || strcmp(tokens[0], "echo") == 0 || strcmp(tokens[0], "touch") == 0) {
        return execute_builtins(tokens);
    }

    return execute_program(tokens);
}

int execute_program(char **tokens) {
    pid_t pid;
    pid_t wait_pid;
    int status;

    pid = fork();
    if (pid == 0) {
        if (execvp(tokens[0], tokens) == -1) {
            perror("no such process exists");
        }
        exit(EXIT_FAILURE);
    }
    else if (pid < 0) {
        perror("fork didn't happen successfully");
    }
    else {
        do {
            wait_pid = waitpid(pid, &status, WUNTRACED);
        }
        while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }

    return 1;
}

int execute_builtins(char **tokens) {
    if (strcmp(tokens[0], "cat") == 0) {
        int token_counter = 0;
        int i = 1;
        while (tokens[i]) {
            token_counter++;
            i++;
        }
        if (token_counter == 0) {
            //should be implemented
        }
        else {
            int c;
            FILE *file;
            for (int j = 1; j < token_counter+1; ++j) {
                file = fopen(tokens[j], "r");
                if (file) {
                    while ((c = getc(file)) != EOF)
                        putchar(c);
                    fclose(file);
                }
                else {
                    printf("no such file or directory");
                }
                printf("\n");
            }
        }
    }
    else if (strcmp(tokens[0], "echo") == 0) {
        int i = 1;
        while (tokens[i] != NULL) {
            if (strcmp(tokens[i], ">")) {
                FILE *fp;
                fp = fopen(tokens[i+2], "w");
                if (fp) {
                    int j = 1;
                    while(j <= i){
                        fprintf (fp, tokens[j]);
                        j++;
                    }
                    fclose (fp);
                }
                else {
                    printf("file didn't specified");
                }
                return 1;
            }
            int j = 0;
            while (tokens[i][j]) {
                if (tokens[i][j] == '\\') {
                    if (j == 0 || tokens[i][j-1] != '\\') {
                        j++;
                        continue;
                    }
                }
                if (tokens[i][j] != '\"' || (tokens[i][j] == '\"' && j > 0 && tokens[i][j-1] == '\\')) {
                    printf("%c", tokens[i][j]);
                }
                j++;
            }
            printf(" ");
            i++;
        }
        printf("\n");
        return 1;
    }
    else if (strcmp(tokens[0], "touch") == 0) {
        int token_counter = 0;
        int i = 1;
        while (tokens[i]) {
            token_counter++;
            i++;
        }
        int c;
        FILE *file;
        for (int j = 1; j < token_counter+1; ++j) {
            file = fopen(tokens[j], "r");
            if (file) {
                char old_file[2048];
                int k = 0;
                while ((c = getc(file)) != EOF) {
                    old_file[k] = c;
                    k++;
                }
                fclose(file);
                file = fopen(tokens[j], "w");
                int result = fputs(old_file, file);
                fclose (file);
            }
            else {
                FILE *fp;
                fp = fopen(tokens[j], "w");
                fclose (fp);
            }
            printf("\n");
        }
        return 1;
    }
}
